This project was initially a non-mandatory part of the C# course that I took. My intention with it was
to use it as an opportunity to gain better understanding of C#, VisualStudio IDE, XML, WPF, client-service 
communication and generally of different programming concepts. This is why the code in this project 
contains some additional packages and methods which were not required, but that I have purposely included
for the sake of learning and practice. These also added either extra funcionality or better user experience
to the project (e.g. methods for creating and writing to an XML document, LiveCharts package, Error alert
messages, default textbox focus,...). I was also trying to make the code easy to read and understand, since
it was to be used as learning material for two of my friends. For this reason, the code includes a mixture 
of both English and Serbian, as well as several comments that I have decided to leave as is.
The project has been tested and works as expected.