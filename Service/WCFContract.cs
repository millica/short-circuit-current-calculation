﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFContract;
using System.Xml;
using System.Xml.Linq;

namespace Service
{
    class WCFContract:IWCFContract
    {
        string docName = "Parametri";        

        // podesavanje forme XML dokumenta (da lepo izgleda, ugnjezdjen i sa razmacima):
        XmlWriterSettings form = new XmlWriterSettings()
        {
            Indent = true,
            IndentChars = "  ",
        };

        public void XMLCreator() // metoda za kreiranje, popunjavanje i citanje XML datoteke
        {
            using (XmlWriter kreator = XmlWriter.Create(docName, form))
            {
                kreator.WriteStartDocument();
                this.XMLWriter(kreator);
                kreator.WriteEndDocument();
            }

            this.XMLReadToVar(); 
        }

        public void XMLWriter(XmlWriter w)
        {
            w.WriteStartElement("parametri");

            InsertValueMreza(w, 35000, 700000000); 
            InsertValueTransformator(w, 0.06, 6000000, 3.5); 
            InsertValueVodovi(w, 0.3, 0.9, 10); 
 
            w.WriteEndElement();
        }

        public void InsertValueMreza(XmlWriter w, int um, int s3pks)
        {
            w.WriteElementString("Um", um.ToString());
            w.WriteElementString("S3pks", s3pks.ToString());
        }

        public void InsertValueTransformator(XmlWriter w, double uk, int sn, double nt)
        {
            w.WriteElementString("uk", uk.ToString());
            w.WriteElementString("Sn", sn.ToString());
            w.WriteElementString("nT", nt.ToString());
        }

        public void InsertValueVodovi(XmlWriter w, double xv, double x0v, int v)
        {
            w.WriteElementString("xv", xv.ToString());
            w.WriteElementString("x0v", x0v.ToString());
            w.WriteElementString("V", v.ToString());
        }

        public void XMLReadToVar() // ova metoda cita konkretne vrednosti za svaki parametar (prvo za mrezu, pa transformator, pa vodove) i upisuje ih u promenljive 
        {
            try
            {
                using (XmlReader citac = XmlReader.Create(docName))
                {
                    int varUm, varS3pks, varSn, varV;
                    double varuk, varnT, varxv, varx0v;

                    while (citac.Read())
                    {
                        if (citac.Name == "parametri")
                        {
                            XElement el = XNode.ReadFrom(citac) as XElement;
                            varUm = Int32.Parse(el.Element(XName.Get("Um")).Value);
                            varS3pks = Int32.Parse(el.Element(XName.Get("S3pks")).Value);

                            varuk = Double.Parse(el.Element(XName.Get("uk")).Value);
                            varSn = Int32.Parse(el.Element(XName.Get("Sn")).Value);
                            varnT = Double.Parse(el.Element(XName.Get("nT")).Value);

                            varxv = Double.Parse(el.Element(XName.Get("xv")).Value);
                            varx0v = Double.Parse(el.Element(XName.Get("x0v")).Value);
                            varV = Int32.Parse(el.Element(XName.Get("V")).Value);
                        }
                    }
                }
            }
            catch
            {
                Console.WriteLine(" Error fetching the parameters.");
            }          
        }

        // sledi implementacija metoda za izracunavanje struja kratkog spoja u odgovarajucim vodovima
        // NAPOMENA: Ovde ubacene vrednosti su samo za proveru, pravu fomulu tek treba izvesti i ubaciti
        // Ove metode vracaju liste vrednosti odgovarajucih struja

        // prvi vod
        public List<double> IzracunajIv1(double d)
        {
            List<double> Iv1 = new List<double>(6){ 
                Math.Pow(0.8 * d, 2),
                Math.Pow(0.7 * d, 2), 
                Math.Pow(0.6 * d, 2), 
                Math.Pow(0.5 * d, 2), 
                Math.Pow(0.4 * d, 2),
                Math.Pow(0.3 * d, 2)}; 
            return Iv1;
        }

        // drugi vod
        public List<double> IzracunajIv2(double d)
        {
            List<double> Iv2 = new List<double>(6){
                Math.Pow(0.1 * d, 2), 
                Math.Pow(0.2 * d, 2), 
                Math.Pow(0.3 * d, 2),
                Math.Pow(0.4 * d, 2), 
                Math.Pow(0.5 * d, 2), 
                Math.Pow(0.6 * d, 2)};
            return Iv2;
        }

        // razlike struja prvog i drugog voda
        public List<double> IzracunajRazliku(List<double> struje1, List<double> struje2)
        {
            List<double> raz = new List<double>(6)
            {
                struje1[0] - struje2[0],
                struje1[1] - struje2[1],
                struje1[2] - struje2[2],
                struje1[3] - struje2[3],
                struje1[4] - struje2[4],
                struje1[5] - struje2[5]
            };
            return raz;
        }
    }
}
