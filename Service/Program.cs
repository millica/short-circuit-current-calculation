﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCFContract;

namespace Service
{
    class Program
    {
        static void Main(string[] args)
        {            
            ServiceHost svc = new ServiceHost(typeof(WCFContract));
            svc.Open();
            Console.WriteLine(" Service running at address: {0}, time: {1}", svc.Description.Endpoints[0].Address, DateTime.Now);
            Console.WriteLine(" Press any key to exit.");
            Console.ReadLine();
            svc.Close();

            //WCFContract test = new WCFContract();
            //test.XMLCreator();

            //Console.Read();
        }
    }
}
