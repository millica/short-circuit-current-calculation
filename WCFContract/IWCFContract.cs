﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;

namespace WCFContract
{
    [ServiceContract]
    public interface IWCFContract
    {

        [OperationContract]
        void XMLCreator();

        [OperationContract]
        void XMLWriter(XmlWriter w);

        [OperationContract]
        void InsertValueMreza(XmlWriter w, int um, int s3pks);

        [OperationContract]
        void InsertValueTransformator(XmlWriter w, double uk, int sn, double nt);

        [OperationContract]
        void InsertValueVodovi(XmlWriter w, double xv, double x0v, int v);

        [OperationContract]
        void XMLReadToVar();

        [OperationContract]
        List<double> IzracunajIv1(double duz);

        [OperationContract]
        List<double> IzracunajIv2(double duz);

        [OperationContract]
        List<double> IzracunajRazliku(List<double> s1, List<double> s2);

    }
}
