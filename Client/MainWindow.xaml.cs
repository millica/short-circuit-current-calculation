﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Wpf;
using WCFContract;

namespace Client
{    public partial class MainWindow : Window
    {
        IWCFContract proxy;
        double duzina;
        CartesianChart ch1, ch2, ch3;

        public MainWindow()
        {
            InitializeComponent();

            ChannelFactory<IWCFContract> factory = new ChannelFactory<IWCFContract>("WCFContract.IWCFContract");
            proxy = factory.CreateChannel();
            Console.WriteLine(" Client started...");

            DockPanelsHide();

            ch1 = CreateChart1();
            ch2 = CreateChart2();
            ch3 = CreateChart3();
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            if (e.Source is Button)
            {
                try
                {
                    duzina = Convert.ToDouble(input.Text);
                    if (duzina < 0.1 || duzina > 30)
                    {
                        MessageBoxImage icon = MessageBoxImage.Exclamation;
                        MessageBoxButton buttonOK = MessageBoxButton.OK;
                        MessageBox.Show("Uneta dužina izlazi iz opsega.", " Greška", buttonOK, icon);
                    }
                    else
                    {
                        Console.WriteLine($" Duzina = {duzina}");
                        UpdateValues(ch1, ch2, ch3, duzina);
                        DockPanelsVisible();
                    }
                }
                catch
                {
                    MessageBoxImage icon = MessageBoxImage.Exclamation;
                    MessageBoxButton buttonOK = MessageBoxButton.OK;
                    MessageBox.Show("Unesite vrednost između 0.1 i 30", " Greška", buttonOK, icon);
                }
            }
        }

        public void DockPanelsHide()
        {
            dockPanel1.Visibility = Visibility.Hidden;
            dockPanel2.Visibility = Visibility.Hidden;
            dockPanel3.Visibility = Visibility.Hidden;
        }

        public void DockPanelsVisible()
        {
            dockPanel1.Visibility = Visibility.Visible;
            dockPanel2.Visibility = Visibility.Visible;
            dockPanel3.Visibility = Visibility.Visible;
        }

        public void UpdateValues(CartesianChart c1, CartesianChart c2, CartesianChart c3, double duz) // crtanje zavisnosti 
        {
            List<double> Iv1 = proxy.IzracunajIv1(duz);
            List<double> Iv2 = proxy.IzracunajIv2(duz);
            List<double> razlika = proxy.IzracunajRazliku(Iv1, Iv2);

            c1.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Iv1 =",
                    Values = new ChartValues<double>(Iv1),
                    Fill = Brushes.Transparent,
                    DataLabels = true,
                    LabelPoint = Point => Convert.ToString(Point.Y) + " A"
                }
            };

            c2.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Iv2 =",
                    Values = new ChartValues<double>(Iv2),
                    Fill = Brushes.Transparent,
                    Stroke = Brushes.Red,
                    DataLabels = true,
                    LabelPoint = Point => Convert.ToString(Point.Y) + " A"
                }
            };

            c3.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Iv1 - Iv2 =",
                    Values = new ChartValues<double>(razlika),
                    PointGeometry = DefaultGeometries.Square,
                    PointGeometrySize = 7,
                    Fill = Brushes.Transparent,
                    Stroke = Brushes.Gold,
                    DataLabels = true,
                    LabelPoint = Point => Convert.ToString(Point.Y) + " A"
                }
            };
        }

        public CartesianChart CreateChart1() // pravimo prvi grafik (blanko, samo sa osama) i smestamo na dockPanel1
        {
            CartesianChart ch1 = new CartesianChart();
            AddAxis(ch1, "Struja prvog voda [A]");
            dockPanel1.Children.Add(ch1);

            return ch1;
        }

        public CartesianChart CreateChart2() // pravimo drugi grafik (blanko, samo sa osama) i smestamo na dockPanel2
        {
            CartesianChart ch2 = new CartesianChart();
            AddAxis(ch2, "Struja drugog voda [A]");
            dockPanel2.Children.Add(ch2);

            return ch2;
        }

        public CartesianChart CreateChart3() // pravimo treci grafik (blanko, samo sa osama) i smestamo na dockPanel3
        {
            CartesianChart ch3 = new CartesianChart();
            AddAxis(ch3, "Razlika struja [A]");
            dockPanel3.Children.Add(ch3);

            return ch3;
        }

        public void AddAxis(CartesianChart c, string TitleY)
        {
            Axis osaX = new Axis();
            osaX.Labels = new List<string> { "0", "Na 20% dužine", "Na 40% dužine", "Na 60% dužine", "Na 80% dužine", "Kraj voda" };
            osaX.Title = "Dužina voda";

            Axis osaY = new Axis();
            osaY.Title = TitleY;

            c.AxisX.Add(osaX);
            c.AxisY.Add(osaY);
        }
    }
}
